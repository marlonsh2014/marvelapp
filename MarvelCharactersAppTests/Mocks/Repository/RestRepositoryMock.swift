//
//  RestRepositoryMock.swift
//  MarvelCharactersAppTests
//
//  Created by Marlon Santos Heitor on 20/01/20.
//  Copyright © 2020 Marlon Santos Heitor. All rights reserved.
//

import Foundation
@testable import MarvelCharactersApp

class RestRepositoryMock: RepositoryProtocol {
    
    private let isToReturnSuccess: Bool
    
    init(isToReturnSuccess: Bool = true) {
        self.isToReturnSuccess = isToReturnSuccess
    }
    
    func loadDataWith<T>(request: BaseRequest, completion: @escaping (Error?, T?) -> Void) where T : BaseResponse {
        
        if (self.isToReturnSuccess) {
            guard let success: T = getSuccessResponse()
                else {return}
            completion(nil, success)
        } else {
            guard let failed: MarvelFailedResponse = getFailedResponse()
                else {return}
            let error = NSError(domain: "Marvel API Request", code: failed.code, userInfo: ["status": failed.status])
            completion(error, nil)
        }
    }
    
    private func getFailedResponse<T>() -> T? where T : BaseResponse {
        guard let data = loadJsonDataWith(fileName: "marvel-api-failed")
            else {return nil}
        guard let failedResponse = try? JSONDecoder().decode(T.self, from: data)
            else {return nil}
        
        return failedResponse
    }
    
    private func getSuccessResponse<T>() -> T? where T : BaseResponse {
        guard let data = loadJsonDataWith(fileName: "marvel-api-success")
            else {return nil}
        guard let successResponse = try? JSONDecoder().decode(T.self, from: data) else {return nil}
        
        return successResponse
    }
    
    private func loadJsonDataWith(fileName: String) -> Data? {
        let bundle = Bundle.init(for: RestRepositoryMock.self)
        guard let filePath = bundle.path(forResource: fileName, ofType: "json")
            else {return nil}
        let url = URL(fileURLWithPath: filePath)
        guard let data = try? Data(contentsOf: url) else {return nil}
        
        return data
    }
    
}
