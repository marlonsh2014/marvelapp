//
//  MarvelCharactersAppTests.swift
//  MarvelCharactersAppTests
//
//  Created by Marlon Santos Heitor on 15/01/20.
//  Copyright © 2020 Marlon Santos Heitor. All rights reserved.
//

import XCTest
@testable import MarvelCharactersApp

class MarvelCharactersAppTests: XCTestCase {

    var modelController: CharactersModelController?
    
    override func setUp() {
        let repository = RestRepositoryMock()
        modelController = CharactersModelController(repository: repository)
    }

    override func tearDown() {
        modelController = nil
    }

    func testGetCharactersModelLimit() {
        let expect = expectation(description: "\(#function)\(#line)")
        guard let modelController = modelController else {return assertionFailure()}
        modelController.fetchCharacters(completion: { (error) in
            if let _ = error {
                assertionFailure()
            }
            
            expect.fulfill()
        })
        
        waitForExpectations(timeout: 10.0) { (error) in
            if let _ = error {
                assertionFailure()
            }
            
            let charactersModel = modelController.getCharacters(limit: 5)
            
            XCTAssertEqual(charactersModel.count, 5)
        }
    }
    
    func testGetCharactersModelOffset() {
        let expect = expectation(description: "\(#function)\(#line)")
        guard let modelController = modelController else {return assertionFailure()}
        modelController.fetchCharacters(completion: { (error) in
            if let _ = error {
                assertionFailure()
            }
            
            expect.fulfill()
        })
        
        waitForExpectations(timeout: 10.0) { (error) in
            if let _ = error {
                assertionFailure()
            }
            
            let charactersModel = modelController.getCharacters(offset: 5)
            
            XCTAssertEqual(charactersModel.count, 95)
        }
    }
    
    func testGetCharactersModelWithError() {
        let repository = RestRepositoryMock(isToReturnSuccess: false)
        let modelController = CharactersModelController(repository: repository)
        
        modelController.fetchCharacters { (error) in
            XCTAssertTrue(error != nil)
        }
    }
    
    func testEmptyArrayWhenNotInitializedModelControllerEntries() {
        guard let modelController = modelController else {return assertionFailure()}
        
        let charactersModelWithOffset = modelController.getCharacters(offset: 5)
        let charactersModelWithLimit = modelController.getCharacters(limit: 5)
        let charactersModel = modelController.getCharacters()

        XCTAssertEqual(charactersModelWithOffset.count, 0)
        XCTAssertEqual(charactersModelWithLimit.count, 0)
        XCTAssertEqual(charactersModel.count, 0)
    }
    
}
