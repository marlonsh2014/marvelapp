//
//  CharacterListModelController.swift
//  MarvelCharactersApp
//
//  Created by Marlon Santos Heitor on 17/01/20.
//  Copyright © 2020 Marlon Santos Heitor. All rights reserved.
//

class CharactersModelController {
    
    private var charactersModel: [CharacterModel]
    private var repository: RepositoryProtocol
    
    private var isDownloading = false
    var limit = 10
    
    internal init(charactersModel: [CharacterModel] = [], repository: RepositoryProtocol) {
        self.charactersModel = charactersModel
        self.repository = repository
    }
    
    func fetchCharacters(completion: @escaping (Error?) -> Void) {
        let timestamp = Constants.MarvelKeys.ts
        let apikey = Constants.MarvelKeys.apikey
        let hash = Constants.MarvelKeys.getMd5Hash()
        let offset = charactersModel.count
        
        if(!isDownloading) {
            isDownloading = true
            let request = CharactersRequest(timestamp: timestamp, apikey: apikey, hash: hash, offset: offset, limit: limit)
            repository.loadDataWith(request: request) { [weak self] (error, marvelModel: MarvelModelResponse?) in
                guard let strongSelf = self else {return}

                if let marvelModel = marvelModel {
                    let charactersModel = marvelModel.toCharectersModel()
                    strongSelf.charactersModel.append(contentsOf: charactersModel)
                }
                
                strongSelf.isDownloading = false
                completion(error)
            }
        }
        
    }
    
    func getCharacters() -> [CharacterModel] {
        return charactersModel
    }
    
    func getCharacters(limit: Int) -> [CharacterModel] {
        return Array(charactersModel.prefix(limit))
    }
    
    func getCharacters(offset: Int) -> [CharacterModel] {
        if (charactersModel.count <= 0) {
            return []
        }
        
        return Array(charactersModel.suffix(from: offset))
    }
    
}
