//
//  CharacterModel.swift
//  MarvelCharactersApp
//
//  Created by Marlon Santos Heitor on 17/01/20.
//  Copyright © 2020 Marlon Santos Heitor. All rights reserved.
//

import Foundation

struct CharacterModel {
    
    let id: Int
    let name, `description`: String
    let imageUrl: String
    
}
