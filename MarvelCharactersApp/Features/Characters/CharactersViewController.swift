//
//  ViewController.swift
//  MarvelCharactersApp
//
//  Created by Marlon Santos Heitor on 15/01/20.
//  Copyright © 2020 Marlon Santos Heitor. All rights reserved.
//

import UIKit

class CharactersViewController: UIViewController {

    @IBOutlet weak var charactersTableView: UITableView!
    @IBOutlet weak var carousel: iCarousel!
    
    private var charactersModelController: CharactersModelController?
    
    private let charactersReuseIdentifier = "characterCell"
    private let carouselReuseIdentifier = "carouselCell"
    
    private var isFirstLoading = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        fetchCharacters()
    }
    
    private func showCollectionsAnimated() {
        showCharactersTableViewAnimated()
        showCarouselViewAnimated()
    }
    
    private func showCharactersTableViewAnimated() {
        let heroListSize = charactersTableView.bounds.size
        charactersTableView.scaleWithBounce(startSize: CGRect.zero.size, endSize: heroListSize)
        charactersTableView.fadeIn()
    }
    
    private func showCarouselViewAnimated() {
        let heroListSize = carousel.bounds.size
        carousel.scaleWithBounce(startSize: CGRect.zero.size, endSize: heroListSize)
        carousel.fadeIn()
    }
    
    private func fetchCharacters() {
        
        charactersModelController?.fetchCharacters() { [weak self] error in
            guard let strongSelf = self else {return}
            
            if error != nil {
                AlertHelper.showError(context: strongSelf, title: "Atenção", message: "O serviço está indisponível no momento.", buttonTitle: "Tentar novamente", isDismissible: !strongSelf.isFirstLoading) {_ in
                    strongSelf.fetchCharacters()
                }
                return
            }
            
            strongSelf.charactersTableView.reloadData()
            if(strongSelf.isFirstLoading) {
                strongSelf.isFirstLoading = false
                strongSelf.carousel.reloadData()
                strongSelf.showCollectionsAnimated()
            }
        }
    }

}

//MARK: - Setup
extension CharactersViewController {
    
    private func setup() {
        setupCharactersModelController()
        setupCarousel()
        setupCharacterTableView()
    }
    
    private func setupCharacterTableView() {
        charactersTableView.alpha = 0
        charactersTableView.layer.cornerRadius = 8
        charactersTableView.dataSource = self
        charactersTableView.delegate = self
        let nib = UINib(nibName: "CharacterTableViewCell", bundle: nil)
        charactersTableView.register(nib, forCellReuseIdentifier: charactersReuseIdentifier)
        charactersTableView.rowHeight = UITableView.automaticDimension
    }
    
    private func setupCarousel() {
        carousel.alpha = 0
        carousel.dataSource = self
        carousel.delegate = self
        carousel.type = .cylinder
    }
    
    private func setupCharactersModelController() {
        let repository = RestRepository(session: URLSession.shared)
        charactersModelController = CharactersModelController(repository: repository)
    }
    
}

//MARK: - iCarousel DataSource & Delegate
extension CharactersViewController: iCarouselDataSource, iCarouselDelegate {
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return getCarouselViewCharacters().count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        let character = getCarouselViewCharacters()[index]
        guard let characterView = UINib(nibName: "CharacterView", bundle: nil)
            .instantiate(withOwner: nil, options: nil).first as? CharacterView else {
                return UIView()
        }
        characterView.setup(character: character)
        
        return characterView
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == iCarouselOption.spacing) {
            return value * 1.2
        }
        return value
    }
    
    private func getCarouselViewCharacters() -> [CharacterModel] {
        guard let modelControler = charactersModelController else {return []}
        return modelControler.getCharacters(limit: 5)
    }
    
}

//MARK: - TableView DataSource & Delegate
extension CharactersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (isSeparator(indexPath.row)) {
            return 10.0
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (isSeparator(indexPath.row)) {
            let separator = UITableViewCell()
            separator.backgroundColor = .clear
            return separator
        }
        
        let index = indexPath.section
        
        let characterModel = getTableViewCharacters()[index]

        guard let cell = tableView.dequeueReusableCell(withIdentifier: charactersReuseIdentifier, for: indexPath) as? CharacterTableViewCell else {
            return UITableViewCell()
        }
        
        cell.setup(character: characterModel)
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return getTableViewCharacters().count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let indexDouble = Double(indexPath.section)
        let modelsCountDouble = Double(getTableViewCharacters().count)
        let percentage = modelsCountDouble * 0.65
        
        if (indexDouble >= percentage) {
            self.fetchCharacters()
        }
    }
    
    private func isSeparator(_ number: Int) -> Bool{
        return number.isMultiple(of: 2)
    }
    
    private func getTableViewCharacters() -> [CharacterModel] {
        guard let modelControler = charactersModelController else {return []}
        return modelControler.getCharacters(offset: 5)
    }
}
