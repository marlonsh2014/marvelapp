//
//  ImageLoader.swift
//  MarvelCharactersApp
//
//  Created by Marlon Santos Heitor on 19/01/20.
//  Copyright © 2020 Marlon Santos Heitor. All rights reserved.
//

import UIKit

class ReusableImageView: UIImageView {
    
    private let imageCache = NSCache<NSString, UIImage>()
    private var imageUrlString: String?
    
    func loadImage(urlString: String, completion: (() -> Void)? = nil) {
        
        imageUrlString = urlString
        
        guard let url = URL(string: urlString) else {
            preconditionFailure()
        }
        
        if let cachedImage = imageCache.object(forKey: urlString as NSString) {
            self.image = cachedImage
            completion?()
            return
        }
        
        DispatchQueue.global(qos: .utility).async {
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        
                        if self.imageUrlString == urlString {
                            self.image = image
                            completion?()
                        }
                        
                        self.imageCache.setObject(image, forKey: urlString as NSString)
                    }
                }
            }
        }
    }
    
}
