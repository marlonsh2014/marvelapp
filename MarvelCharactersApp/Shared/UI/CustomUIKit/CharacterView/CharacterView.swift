//
//  CharacterView.swift
//  MarvelCharactersApp
//
//  Created by Marlon Santos Heitor on 19/01/20.
//  Copyright © 2020 Marlon Santos Heitor. All rights reserved.
//

import UIKit

class CharacterView: UIView {

    @IBOutlet weak var characterImageView: ReusableImageView!
    @IBOutlet weak var characterTitleLabel: UILabel!
    
    func setup(character: CharacterModel) {
        
        characterTitleLabel.text = nil
        characterImageView.image = nil
        characterImageView.loadImage(urlString: character.imageUrl, completion: { [weak self] in
            guard let strongSelf = self else {return}
            
            strongSelf.characterTitleLabel.attributedText = NSMutableAttributedString(string: character.name, attributes: strongSelf.getOutlinedTextAttributes())
            
        })
    }
    
    private func getOutlinedTextAttributes() -> [NSAttributedString.Key : Any] {
        let strokeTextAttributes = [
        NSAttributedString.Key.strokeColor : UIColor.black,
        NSAttributedString.Key.foregroundColor : UIColor.white,
        NSAttributedString.Key.strokeWidth : -5.0,
        NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 20)]
        as [NSAttributedString.Key : Any]
        
        return strokeTextAttributes
    }
}
