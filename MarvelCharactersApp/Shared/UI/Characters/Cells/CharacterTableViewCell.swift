//
//  CharacterTableViewCell.swift
//  MarvelCharactersApp
//
//  Created by Marlon Santos Heitor on 19/01/20.
//  Copyright © 2020 Marlon Santos Heitor. All rights reserved.
//

import UIKit

class CharacterTableViewCell: UITableViewCell {

    @IBOutlet weak var characterImage: ReusableImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(character: CharacterModel) {
        characterImage.loadImage(urlString: character.imageUrl)
        titleLabel.text = character.name
        descriptionLabel.text = character.description
        
        self.layer.cornerRadius = 10
        self.backgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.7)
    }
    
    override func prepareForReuse() {
        characterImage.image = nil
    }
    
}
