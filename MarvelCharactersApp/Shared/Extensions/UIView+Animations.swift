//
//  UIView+Animations.swift
//  MarvelCharactersApp
//
//  Created by Marlon Santos Heitor on 16/01/20.
//  Copyright © 2020 Marlon Santos Heitor. All rights reserved.
//

import UIKit

extension UIView {
    func scaleWithBounce(startSize: CGSize, bounceIncrement: CGFloat = 5.0, endSize: CGSize) {
        self.bounds.size = startSize
        UIView.animate(withDuration: 0.5, animations: {
            self.bounds.size.width = endSize.width + bounceIncrement
            self.bounds.size.height = endSize.height + bounceIncrement
        }) { (completed) in
            UIView.animate(withDuration: 0.3) {
                self.bounds.size.width = endSize.width
                self.bounds.size.height = endSize.height
            }
        }
    }
    
    func fadeIn(duration: Double = 1.0, alphaValue: CGFloat = 1.0) {
        UIView.animate(withDuration: duration) {
            self.alpha = alphaValue
        }
    }
}
