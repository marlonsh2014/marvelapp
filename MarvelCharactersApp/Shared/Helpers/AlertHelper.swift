//
//  AlertHelper.swift
//  MarvelCharactersApp
//
//  Created by Marlon Santos Heitor on 18/01/20.
//  Copyright © 2020 Marlon Santos Heitor. All rights reserved.
//

import UIKit

class AlertHelper: NSObject {

    static func showError(context: UIViewController, title: String, message: String, buttonTitle: String, isDismissible: Bool = true, handler: @escaping (UIAlertAction) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: handler))
        if (isDismissible) {
            alert.addAction(UIAlertAction(title: "Fechar", style: .default, handler: nil))
        }

        context.present(alert, animated: true, completion: nil)
    }
    
}
