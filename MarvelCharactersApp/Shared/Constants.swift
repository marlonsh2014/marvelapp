//
//  Constants.swift
//  MarvelCharactersApp
//
//  Created by Marlon Santos Heitor on 18/01/20.
//  Copyright © 2020 Marlon Santos Heitor. All rights reserved.
//

import Foundation
import CryptoSwift

struct Constants {
    
    static let indicatorTag = -22876
    
    struct MarvelKeys {
        static let ts = "1"
        static let apikey = "f58a8f21c87974445771a8fee20975c3"
        static let privateKey = "ee65681f9dc0913037cfbdacf8e4260571ed6f99"
        
        static func getMd5Hash() -> String {
            return "\(ts)\(privateKey)\(apikey)".md5()
        }
    }
}


