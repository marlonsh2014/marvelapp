//
//  CharactersRequest.swift
//  MarvelCharactersApp
//
//  Created by Marlon Santos Heitor on 16/01/20.
//  Copyright © 2020 Marlon Santos Heitor. All rights reserved.
//

import Foundation

struct CharactersRequest: BaseRequest {
    
    let timestamp: String
    let apikey: String
    let hash: String
    let offset: Int
    let limit: Int
    
    var url: String {
        
        let limitStringSuffix = limit > 0 ? "&limit=\(limit)" : ""
        let offsetStringSuffix = offset > 0 ? "&offset=\(offset)" : ""

        return "https://gateway.marvel.com:443/v1/public/characters?ts=\(timestamp)&apikey=\(apikey)&hash=\(hash)\(offsetStringSuffix)\(limitStringSuffix)"
    }
}
