//
//  BaseRequest.swift
//  EasynvestTest
//
//  Created by Marlon Santos Heitor on 30/11/19.
//  2019 Marlon Santos Heitor. All rights reserved.
//

import Foundation

protocol BaseRequest {
    var url: String { get }
}
