//
//  MarvelResponse.swift
//  MarvelCharactersApp
//
//  Created by Marlon Santos Heitor on 16/01/20.
//  Copyright © 2020 Marlon Santos Heitor. All rights reserved.
//

import Foundation

struct MarvelModelResponse: BaseResponse {
    let data: DataClass
    
    func toCharectersModel() -> [CharacterModel] {
        let charactersModel = self.data.results.map { (characterData) -> CharacterModel in
            let id = characterData.id
            let name = characterData.name
            let description = characterData.description
            let thumbnail = characterData.thumbnail
            let imageUrl = "\(thumbnail.path).\(thumbnail.extension)"
            
            return CharacterModel(id: id, name: name, description: description, imageUrl: imageUrl)
        }
        return charactersModel
    }
}

struct DataClass: Codable {
    let results: [Result]
}

struct Result: Codable {
    let id: Int
    let name, `description`: String
    let modified: String?
    let thumbnail: Thumbnail
}

struct Thumbnail: Codable {
    let path: String
    let `extension`: Extension
}
enum Extension: String, Codable {
    case gif = "gif"
    case jpg = "jpg"
    case png = "png"
}
