//
//  MarvelFailedResponse.swift
//  MarvelCharactersApp
//
//  Created by Marlon Santos Heitor on 20/01/20.
//  Copyright © 2020 Marlon Santos Heitor. All rights reserved.
//

import Foundation

struct MarvelFailedResponse: BaseResponse {
    let code: Int
    let status: String
}
