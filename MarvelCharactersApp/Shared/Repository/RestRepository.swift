//
//  RestRepository.swift
//  EasynvestTest
//
//  Created by Marlon Santos Heitor on 30/11/19.
//  2019 Marlon Santos Heitor. All rights reserved.
//

import Foundation

protocol RepositoryProtocol {
    func loadDataWith<T: BaseResponse>(request: BaseRequest, completion: @escaping (Error?, T?) -> Void)
}

class RestRepository: RepositoryProtocol {
    
    private var session: URLSession
    
    init(session: URLSession) {
        self.session = session
    }
    
    func loadDataWith<T: BaseResponse>(request: BaseRequest, completion: @escaping (Error?, T?) -> Void) {
        
        guard let url = URL(string: request.url) else {
            preconditionFailure()
        }

        let request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                 timeoutInterval: 10.0)
        
        session.dataTask(with: request) { (data, _, serviceError) in
            DispatchQueue.main.async {
                guard serviceError == nil else {
                    completion(serviceError, nil)
                    return
                }
                do {
                    let responseObject = try JSONDecoder().decode(T.self, from: data!)
                    completion(nil, responseObject)
                } catch {
                    completion(error, nil)
                }
            }
        }.resume()
    }
}
