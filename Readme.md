# Marvel Characters App (A good app name)

This project shows all the Marvel characters, in a carousel and list components.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
- Git - https://git-scm.com/downloads
- Carthage - https://github.com/Carthage/Carthage#installing-carthage
- Xcode - https://apps.apple.com/br/app/xcode/id497799835
```

### Installing

A step by step series of examples that tell you how to get a development env running

```
1. Open the terminal
2. Use git to clone the project
3. Get into the project root directory
4. Install carthage dependencies
5. Open ".xcproject" file
6. Run the project
```

Run the script below to get into the project (Make sure you have write permission to the directory)

```
git clone https://marlonsh2014@bitbucket.org/marlonsh2014/marvelapp.git &&
cd marvelapp &&
carthage update &&
open MarvelCharactersApp.xcodeproj
```

### Throubleshooting

Carthage Build Error

```
- Make sure you have an up to date version of Carthage
```


## Authors

* Marlon Santos Heitor


